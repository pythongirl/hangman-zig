const std = @import("std");
const builtin = std.builtin;
const Allocator = std.mem.Allocator;
const Writer = std.fs.File.Writer;
const Reader = std.fs.File.Reader;

const BAD_GUESSES_ALLOWED = 10;

fn check_word(word: []const u8) bool {
    return for(word) |letter| {
        if(!std.ascii.isLower(letter)) break false;
    } else true;
}

fn pick_word(allocator: *Allocator) ![]const u8 {
    comptime const dictionary = @embedFile("/usr/share/dict/usa");

    var words = std.ArrayList([]const u8).init(allocator);
    defer words.deinit();

    var pos: usize = 0;
    var word_start: usize = 0;
    while(pos < dictionary.len): (pos += 1){
        const byte = dictionary[pos];
        if(byte == '\n'){
            const word = dictionary[word_start..pos];
            if(check_word(word)){
                try words.append(word);
            }
            word_start = pos + 1;
        }
    }

    const seed =  std.crypto.random.int(u64);

    var rng = std.rand.DefaultPrng.init(seed);

    const my_word = words.items[rng.random.int(usize) % words.items.len];

    // return allocator.dupe(u8, my_word);
    return my_word;
}

fn display_word(output: *Writer, word: []const u8, guessed_letters: *const [26]bool) !void {
    try output.print("\n", .{});
    for (word) |letter| {
        if(guessed_letters[letter - 'a']){
            try output.print(" {c}", .{letter});
        } else {
            try output.print(" _", .{});
        }
    }
    try output.print("\n\n", .{});
}

fn read_guess(input: *Reader, output: *Writer) !u8 {
    var guess: u8 = undefined;
    try output.print("Guess a letter: ", .{});

    comptime const delimiter = switch(builtin.os.tag){
        .windows => '\r',
        else => '\n',
    };

    if(input.readUntilDelimiterOrEof(@ptrCast(*[1]u8, &guess), delimiter)) |result|{
        if(builtin.os.tag == .windows){
            const newline = try input.readByte();
        }

        if(result) |guess_slice| {
            if(guess_slice.len == 1){
                if(std.ascii.isAlpha(guess)){
                    return std.ascii.toLower(guess);
                } else {
                    try output.print("Please guess a letter.\n", .{});
                    return read_guess(input, output);
                }
            } else {
                try output.print("Please guess a single letter.\n", .{});
                return read_guess(input, output);
            }
        } else {
            return error.EndOfStream;
        }
    } else |err| {
        switch(err){
            error.StreamTooLong => {
                try input.skipUntilDelimiterOrEof('\n');
                try output.print("Please guess a single letter at a time.\n", .{});
                return read_guess(input, output);
            },
            else => |e| return e,
        }
    }
}

fn is_won(word: []const u8, guessed_letters: *const [26]bool) bool {
    return for(word) |letter| {
        if(guessed_letters[letter - 'a'] == false) break false;
    } else true;
}

const HangmanError = error{WordCommandFailed};

pub fn main() !void {
    var stdout = std.io.getStdOut().writer();
    var stdin = std.io.getStdIn().reader();
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};

    const word = try pick_word(&gpa.allocator);

    // var inputBuffer = std.ArrayList(u8).init(&gpa.allocator);
    var guessed_letters: [26]bool = [_] bool{false} ** 26;
    var bad_guesses_left: usize = BAD_GUESSES_ALLOWED;

    try display_word(&stdout, word, &guessed_letters);
    while(bad_guesses_left > 0){
        const guess = read_guess(&stdin, &stdout) catch |err| {
            switch(err){
                error.EndOfStream => {
                    try stdout.print("Goodbye.\n", .{});
                    return;
                },
                else => |e| return e,
            }
        };

        if(guessed_letters[guess - 'a'] == true){
            try stdout.print("You've already used that letter.\n", .{});
            continue;
        }
        guessed_letters[guess - 'a'] = true;

        const is_successful_guess = for(word)|l| {
            if(l == guess) break true;
        } else false;

        try display_word(&stdout, word, &guessed_letters);

        if(is_successful_guess){
            if(is_won(word, &guessed_letters)){
                try stdout.print("\u{1f389} You Win! \u{1f389}\n\u{1f44f}\u{1f44f}\u{1f44f}\u{1f44f}\u{1f44f}\u{1f44f}\u{1f44f}\n", .{});
                return;
            }
        } else {
            bad_guesses_left -= 1;
            try stdout.print("Incorrect. You have {} wrong guesses left.\n", .{bad_guesses_left});
        }

        try stdout.print("Letters guessed:", .{});
        for(guessed_letters) |is_guessed, index| {
            if(is_guessed){
                try stdout.print(" {c}", .{@intCast(u8, 'a' + index)});
            }
        }
        try stdout.print("\n", .{});
    }

    try stdout.print("\u{1f622} You lost. The word was \"{s}\".\n", .{word});
}
